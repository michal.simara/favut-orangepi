#!/bin/bash

# initialization of system variables
source /etc/environment

# debug print of system variables
#echo ${FAVUT_DIR}
#echo ${WIFI_NAME}
#echo ${WIFI_INTERFACE}

# this script use create_ap from https://github.com/oblique/create_ap
# check if wifi AP is running
running=$(${FAVUT_DIR}create_ap-master/create_ap --list-running | grep -c wlan0)

# debug print of runig state
echo "running $running"

# if AP is not running start it
if [ $running -eq 0 ];
then
	${FAVUT_DIR}create_ap-master/create_ap -n wlan0 $WIFI_NAME FAVUT007 > /home/mike/start_ap.log 2>&1 &  # start ap on interface wlan0 with name in var WIFI_NAME and psw FAVUT007
fi

# starts file browser service that creates web client for accesing files on orange pi in folder "/home/mike/data"
# more about filebrowser https://filebrowser.github.io/ default user: "admin" and psw: "admin" 
filebrowser --port 70 -s /home/mike/data 2> /home/mike/filebrowser.log &
#filebrowser --port 70 -a 192.168.12.1 -r /home/mike/data &

