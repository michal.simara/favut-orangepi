#!/bin/bash

# Load environment variables
source /etc/environment

# Clear wireshark cache - without it could application crash again after previous crash
rm -f /tmp/*wireshark*.pcapng

# starts sniff with tshark on tplink wifi interface 
# more about tshark: https://www.wireshark.org/docs/man-pages/tshark.html
# more about tplink device: https://www.tp-link.com/en/products/details/cat-11_TL-WN722N.html 
# tplink TL-WN722N tested with version v1 and v1.10 that has drivers with monitor mode support
bash ${FAVUT_DIR}tshark_tplink.sh 2> /home/mike/error1.log

# check if script that displays stats and display module is running
# bash ${FAVUT_DIR}start_display.sh &

