#!/bin/bash

today=`date +%Y-%m-%d-%H-%M-%S`

tshark -i eth0 -f "udp port 37008" -Y "wlan.fc.type_subtype==0x04" -Tfields -e frame.number -e frame.time -e frame.time_relative -e wlan.sa -e tzsp.wlan.channel -e tzsp.wlan.signal -E header=y -E separator=";" > /home/mike/data/sniff_log$today.csv &

