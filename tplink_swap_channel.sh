#!/bin/bash

# scripts that swaps wifi channel that is sniffed
# I am not sure if it is necessary - should be tested

# initialization of system variables
source /etc/environment

# list of channels
channels=( 1 2 3 4 5 6 7 8 9 10 11 12 13 14 )

# each 100ms swap wifi channel
while true
do
  for ch in "${channels[@]}"
  do
    /sbin/iwconfig $WIFI_INTERFACE channel $ch  > /dev/null 2>&1
    sleep 0.10
  done
done

