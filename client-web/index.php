
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <script>
    setTimeout(function(){
      window.location.reload(1);
    }, 5000);
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Orange PI wifi-data</title>
  </head>
  <body>
    <?php
      $file = '/home/mike/data/' . shell_exec('ls -tr /home/mike/data | tail -n 1');
      $file = str_replace("\n", "", $file);          
    ?>
    <div class="main_page">
      <div class="stats">
        <b><label style="font-size: 40px" for="unique_count">Unique count: </label></b>
        <span style="font-size: 50px" id="unique_count">
        <?php
          $cmd_unique_count = 'cat '.$file.' | awk -F "\"*;\"*" \'{print $4}\' | sort | uniq | wc -l';
          $people_count = shell_exec($cmd_unique_count);
          echo "$people_count";
        ?>
        </span><br>        
        <b><label style="font-size: 40px" for="complete_count">Complete count: </label></b>
        <span style="font-size: 50px" id="complete_count">
        <?php
          $cmd_complete_count = 'cat '.$file.' | wc -l';
          $complete_count = shell_exec($cmd_complete_count);
          echo "$complete_count";
        ?>        
        </span><br>
        <?php
          $date_string = shell_exec('cat '.$file.' | awk -F "\"*;\"*" \'{print $2}\' | head -n -1 | tail -n 1');
          $date_string = str_replace("\n", "", $date_string);
          $last_time = DateTime::createFromFormat('M d, Y H:i:s.u??? e', $date_string, new DateTimeZone( '+0000' ));
          $last_time->setTimeZone(new DateTimeZone( '+0200' ));
          $date = $last_time->format("d m Y");
          $time = $last_time->format("H:i:s");         
        ?>        
        <b><label style="font-size: 40px" for="last-date">Last date: </label></b>
        <span style="font-size: 50px" id="last-date">
        <?php
          echo "$date";
        ?>
        </span><br>
        <b><label style="font-size: 40px" for="last-time">Last time: </label></b>
        <span style="font-size: 50px" id="last-time">
        <?php
          echo "$time";
        ?>                        
	</span><br>
        <b><label style="font-size: 40px" for="act-time">Act time: </label></b>
        <span style="font-size: 50px" id=act-time">
        <?php
           echo shell_exec('date "+%H:%M:%S %d.%m. %Y"');
        ?>
        </span><br>
      </div>
    </div>
  </body>
</html>

