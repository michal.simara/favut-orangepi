#!/usr/bin/env python

# Script that updates statistics from sniffer and using python library https://github.com/BLavery/lib_oled96

from lib_oled96 import ssd1306

from smbus import SMBus
import time
import subprocess


def update_time(oled):
    # we are ready to do some output ...
    # put border around the screen:
    oled.canvas.rectangle((0, 0, oled.width-1, 15), outline=0, fill=0)
    # Write two lines of text.
    oled.canvas.text((5,0),    time.strftime("%d.%m.%Y %H:%M:%S", time.localtime()), fill=1)
    # now display that canvas out to the hardware

def update_stats(oled):
    #print("Updating stats")
    proc=subprocess.Popen(['/bin/bash', '/home/mike/data/export_stats.sh'], stdout=subprocess.PIPE)
    line = proc.stdout.readline()
    values = line.split(";")
    #print(values)
    if( len(values) == 4):
        people_count="People: " + values[0]
        complete_count="Complete: " + values[1]
        last_time=values[2]
        oled.canvas.rectangle((0, 16, oled.width-1, oled.height-1), outline=0, fill=0)
        oled.canvas.text((0, 15), people_count, fill=1)
        oled.canvas.text((0, 27), complete_count, fill=1)
        oled.canvas.text((0, 39), "Last: ", fill=1)
        oled.canvas.text((0, 51), last_time, fill=1)
    elif line == 'ERROR':
        oled.canvas.rectangle((0, 16, oled.width-1, oled.height-1), outline=0, fill=0)
        oled.canvas.text((0,30), 'Error: zarizeni se restartuje', fill=1)
        proc=subprocess.Popen(['/bin/bash', '/home/mike/scripts/favut-orangepi/restart_sniff.sh'])
    
i2cbus = SMBus(1)        # 1 = Raspberry Pi but NOT early REV1 board
oled = ssd1306(i2cbus)   # create oled object, nominating the correct I2C bus, default address

i = 0;
while True:
    update_time(oled)
    if i%50==0:
        update_stats(oled)
        i=0
    oled.display()
    i+=1
    time.sleep(0.1)

