Actual working cnfiguration:
 - mini-pc: orange pi zero (v1.5) viz http://www.orangepi.org/orangepizero/
 - wifi module: tp-link TL-WN722N (v1 and v1.10)
 - rtc module: DS3231 viz https://www.banggood.com/3PCS-DS3231-Clock-Module-3_3V-5V-High-Accuracy-For-Raspberry-Pi-p-1146720.html?rmmds=myorder&cur_warehouse=CN
 - OLED display: 0.96 Inch Blue Yellow IIC I2C OLED viz https://www.banggood.com/3Pcs-0_96-Inch-Blue-Yellow-IIC-I2C-OLED-Display-Module-For-Arduino-p-1155452.html?rmmds=myorder&cur_warehouse=CN

Required software:
 - tshark - https://www.wireshark.org/docs/man-pages/tshark.html
 - create_ap - https://github.com/oblique/create_ap
 - lib_oled96 - https://github.com/BLavery/lib_oled96
 - file browser - https://filebrowser.github.io/

Configuration of system:

1. Initialization of system - setting system variables in file:
/etc/environment

WIFI_NAME=FAVUT_X # replace X with id number of orange pi
WIFI_INTERFACE=<interface_name> # replace <interface_name> with wifi interface name that can be switched into monitor mode
FAVUT_DIR=/home/mike/scripts/favut-orangepi/ # replace with scripts home folder

2. Configuration of wifi sniffer:
- edit file: /etc/network/interfaces and add:
allow-hotplug <wlan-interface> # replace <wlan-interface> with wifi interface name that supports monitor mode
iface <wlan-interface> inet dhcp
        post-up <start-sniff-script> # replace <start-sniff-script> with path to start_sniff.sh

3. Configuration of wifi AP:
- edit file: /etc/network/interfaces and add:
allow-hotplug wlan0 # wlan 0 should be embedede wifi device of orange pi
iface wlan0 inet dhcp
        post-up <start-ap-script> # replace <start-ap-script> with path to start_ap.sh

4. Inicialization of RTC (real-rime clocks) modul:
- edit file: /etc/rc.local and add:
echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-0/new_device
sleep 10
hwclock -s -f /dev/rtc1

- edit file /etc/modules-load.d/modules.conf and add i2c device:
i2c-bcm2708
i2c-dev

