<?php

  function isDeviceConnected($deviceIp) {
     return exec("arp | grep -e ".$deviceIp."\\\\s | wc -l") == '1';
  }

  function convertToStatus($connected) {
     return $connected ? 'Connected' : 'Disconnected';
  }

  $project = exec('less actual_project.txt');

  $result->devices = array();

  $favut1->connected = isDeviceConnected('192.168.10.11');
  $favut1->status = convertToStatus($favut1->connected);
  $favut1->time = json_decode(exec('curl 192.168.10.11/time.php'))->time;

  $favut2->connected = isDeviceConnected('192.168.10.12');
  $favut2->status = convertToStatus($favut2->connected);
  $favut2->time = json_decode(exec('curl 192.168.10.12/time.php'))->time;

  $favut3->connected = isDeviceConnected('192.168.10.13');
  $favut3->status = convertToStatus($favut3->connected);
  $favut3->time = json_decode(exec('curl 192.168.10.13/time.php'))->time;

  $result->devices['favut1'] = $favut1;
  $result->devices['favut2'] = $favut2;
  $result->devices['favut3'] = $favut3;

  exec('echo "'.date('Y-m-d H:i:s').';'.$favut1->time.';'.$favut2->time.';'.$favut3->time.';" >> projects/'.$project);

  echo json_encode($result);

?>
