
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>FAVUT RPI</title>
    <script src="jQuery.min.js"></script>
    <script>
      function synchronize() {
        $.ajax(
	  {
             url: "synchronize.php",
	     data: { project: $("#project").val() },
             dataType: "json",
	     success: function(result){
               $("#synchronized").html('synchronized');
	     }
          });
      }

      function refreshTime() {
        $.ajax(
          {
            url: "time.php",
            dataType: "json",
            success: function(result){
              $("#time").html(result.time);
            }
          });    
      }

      function refreshDeviceStatus() {
        $.ajax(
          {
            url: "device_status.php",
            dataType: "json",
	    success: function(result) {
               $("#favut1-status").html(result.devices['favut1'].status);
	       $("#favut1-time").html(result.devices['favut1'].time);
	       $("#favut2-status").html(result.devices['favut2'].status);
	       $("#favut2-time").html(result.devices['favut2'].time);
	       $("#favut3-status").html(result.devices['favut3'].status);
	       $("#favut3-time").html(result.devices['favut3'].time);
	    }
          });
      }

      setInterval(refreshTime, 5000);
      setInterval(refreshDeviceStatus, 5000);
    </script>
  </head>
  <body>
    <div>
      <label for="time">Actual time:</label>
      <span id="time"/>
    </div>
    <div>
      <label for="project">Project:</label>
      <input id="project" type="text"/>
    </div>
    <div>
      <button type="button" onclick="synchronize()">Synchronize</button>
      <span id="synchronized"/>
    </div>
    <div>
      <span>Favut1</span>
      <div>
        <label for="favut1-status">State:</label>
	<span id="favut1-status" />
      </div>
      <div>
        <label for="favut1-time">Time:</label>
	<span id="favut1-time" />
      </div>
    </div>
    <div>
      <span>Favut2</span>
      <div>
	<label for="favut2-status">State:</label>
	<span id="favut2-status" />
      </div>
      <div>
        <label for="favut2-time">Time:</label>
        <span id="favut2-time" />
      </div>
    </div>
    <div>
      <span>Favut3</span>
      <div>
	<label for="favut3-status">State:</label>
	<span id="favut3-status" />
      </div>
      <div>
        <label for="favut3-time">Time:</label>
        <span id="favut3-time" />
      </div>
    </div>
  </body>
</html>

