#!/bin/bash

# Load environment variables
source /etc/environment

while : 
do
  displayRunning="$(ps -ax | grep 'wifi-display-stats' | grep -cv 'grep')"
  #echo "Display is running: " $displayRunning
  if [ $displayRunning -eq 0 ];
  then
    #echo "Starting display."
    sudo python ${FAVUT_DIR}LCD/wifi-display-stats.py &
  fi
  sleep 5;
done
