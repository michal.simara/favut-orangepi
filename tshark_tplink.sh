#!/bin/bash

# Initialization of system variables
source /etc/environment

hwclock -s -f /dev/rtc1 

# Read actual date and time and store to variable
today=`date +%Y-%m-%d-%H-%M-%S`
echo "today: " $today

# Check if script is running
tsharkRunning="$(ps -ax | grep 'wireshark' | grep -cv 'grep')"

# Debug print actual state
echo "tshark running: $tsharkRunning"

# If sniffer is not runnng start it
if [ $tsharkRunning -eq 0 ];
then
    echo "starting tshark"
    # start as user "wireshark" and with turned of buffer so output stream is immidiately written into output file
    sudo -u wireshark unbuffer tshark -i $WIFI_INTERFACE -I -f 'type mgt subtype probe-req' -Y "wlan.fc.type_subtype==0x04" -Tfields -e frame.number -e frame.time -e frame.time_relative -e wlan.sa -e wlan_radio.channel -e wlan_radio.signal_dbm -e wlan.sa_resolved -E header=y -E separator=";" > /home/mike/data/sniff_log$today.csv 2>> /home/mike/tshark_error.log & 
    echo "tshark started"
fi

# Check if script for swaping channels is running
#swpchRunning="$(ps -ax | grep 'tplink_swap_channel' | grep -cv 'grep')"

# Debug print actual state
#echo "swap running: $swpchRunning"

# If swap channel script is not running start it
#if [ $swpchRunning -eq 0 ];
#then
#	bash ${FAVUT_DIR}tplink_swap_channel.sh &
#fi

