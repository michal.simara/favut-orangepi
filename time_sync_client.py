#!/usr/bin/python3
# https://pymotw.com/2/socket/tcp.html
import socket
import sys
import os
import datetime
import time

log = open('/tmp/time_sync_client.log', 'w', buffering=1)

def startTimeSyncClient():
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connect the socket to the port where the server is listening
    server_address = ('192.168.10.1', 10000)
    sock.connect(server_address)

    log.write('Client connected.\n')

    try:
        # Send data
        device = sys.argv[1]
        sock.sendall(device.encode())

        log.write('Client introduced.\n')

        while True:
            serverTime = sock.recv(32).decode()
            log.write('Client receive time from server: '+ serverTime +'.\n')
            os.system('echo favut007 | sudo -S date -s "' + serverTime + '"')
            deviceTime = datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S.%f')
            sock.sendall(deviceTime.encode())
            log.write("Client sent own synced time.\n")
            os.system('echo favut007 | sudo -S /home/mike/scripts/favut-orangepi/restart_sniff.sh')
    finally:
        sock.close()

while True:
  try:
    startTimeSyncClient()
  except:
    time.sleep(5)
