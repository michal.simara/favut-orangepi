#!/bin/bash

# script used for restarting sniff

# initialization of environment variables
source /etc/environment

# killing old processes
kill -9 $(ps aux | grep 'tshark' | grep -v 'grep' | awk '{print $2}')
#kill -9 $(ps aux | grep 'tplink_swap_channel.sh' | grep -v 'grep' | awk '{print $2}')
#kill -9 $(ps aux | grep 'wifi-display-stats.py' | grep -v 'grep' | awk '{print $2}')

# starting sniff again
bash ${FAVUT_DIR}start_sniff.sh



